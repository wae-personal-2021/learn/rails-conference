class CreatePapers < ActiveRecord::Migration[6.1]
  def change
    create_table :papers do |t|
      t.string :title
      t.text :abstract
      t.date :submission_date
      t.string :pdf_link
      t.string :decision
      t.references :users, null: false, foreign_key: true

      t.timestamps
    end
  end
end
