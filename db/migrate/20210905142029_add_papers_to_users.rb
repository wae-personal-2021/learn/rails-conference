class AddPapersToUsers < ActiveRecord::Migration[6.1]
  def change
    add_reference :users, :paper, null: false, foreign_key: true
  end
end
