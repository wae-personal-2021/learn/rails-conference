class AddCanReviewToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :can_review, :boolean, default: false, null: false
  end
end
