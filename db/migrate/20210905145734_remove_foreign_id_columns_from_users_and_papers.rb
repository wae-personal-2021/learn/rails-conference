class RemoveForeignIdColumnsFromUsersAndPapers < ActiveRecord::Migration[6.1]
  def change
    remove_reference :papers, :user, index: true, foreign_key: true
    remove_reference :users, :paper, index: true, foreign_key: true
  end
end
