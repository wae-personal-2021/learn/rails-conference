class CreatePaperUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :paper_users do |t|
      t.references :paper, null: false, foreign_key: true
      t.references :user, null: false, foreign_key: true
      t.integer :author_order

      t.timestamps
    end
  end
end
