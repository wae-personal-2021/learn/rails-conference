class ChangeDateToDatetimeInPapers < ActiveRecord::Migration[6.1]
  def change
    change_column :papers, :submission_date, :datetime
  end
end
