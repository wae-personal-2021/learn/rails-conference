class RemoveUsersIdFromPapers < ActiveRecord::Migration[6.1]
  def change
    remove_column :papers, :users_id, :integer
  end
end
