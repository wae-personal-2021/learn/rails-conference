class RemoveSubmissionDateFromPapers < ActiveRecord::Migration[6.1]
  def change
    remove_column :papers, :submission_date
  end
end
