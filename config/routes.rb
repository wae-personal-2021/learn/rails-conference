Rails.application.routes.draw do

  root 'home#index'

  get 'all_papers', to: 'home#all_papers', as: :all_papers
  get 'about', to: 'home#about', as: :about

  
  devise_for :users

  devise_scope :user do
    delete 'sign_out', to: 'devise/sessions#destroy', as: :sign_out
    get 'sign_in', to: 'devise/sessions#new', as: :sign_in
    get 'sign_up', to: 'devise/registrations#new', as: :sign_up
  end

  resources :papers

  # resources :users do
  #   resources :papers
  # end

  # resources :papers do
  #   resources :users
  # end
  
  


  # get '/all_papers', to: 'papers#all', as: :all_papers
  # resources :papers
end
