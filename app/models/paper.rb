class Paper < ApplicationRecord
  has_many :paper_users
  has_many :users, through: :paper_users

  def submission_date
    return updated_at.localtime.to_formatted_s(:long)
  end
end
