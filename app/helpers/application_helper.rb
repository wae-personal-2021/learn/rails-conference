module ApplicationHelper
    def active_class(path)
        return request.path == path ? 'nav-link active' : 'nav-link'
    end
end
