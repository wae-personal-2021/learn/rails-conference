json.extract! paper, :id, :title, :abstract, :submission_date, :pdf_link, :decision, :users_id, :created_at, :updated_at
json.url paper_url(paper, format: :json)
