class PapersController < ApplicationController
  before_action :set_paper, only: %i[ show edit update destroy ]
  before_action :authenticate_user!, except: [:show, :all]
  before_action :valid_paper_of_current_user, only: [:show, :edit, :update, :destroy]

  def all
    @papers = Paper.all
  end

  # GET /user/1/papers
  def index
    @papers = current_user.papers.all
  end

  # GET /papers/1 or /papers/1.json
  def show
    @user = current_user
  end

  # GET /papers/new
  def new
    # @paper = Paper.new
    @paper = current_user.papers.new
  end

  # GET /papers/1/edit
  def edit
  end

  # POST /papers or /papers.json
  def create
    respond_to do |format|
      if current_user.papers.create(paper_params)
        format.html { redirect_to papers_url, notice: "Paper was successfully created." }
        format.json { render :show, status: :created, location: @paper }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @paper.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /papers/1 or /papers/1.json
  def update
    respond_to do |format|
      if @paper.update(paper_params)
        format.html { redirect_to @paper, notice: "Paper was successfully updated." }
        format.json { render :show, status: :ok, location: @paper }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @paper.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /papers/1 or /papers/1.json
  def destroy
    @paper = Paper.find(params[:id])
    respond_to do |format|
      if current_user.papers.destroy(@paper)
        format.html { redirect_to papers_url, notice: "Paper was successfully destroyed." }
        format.json { head :no_content }
      else
        format.html { redirect_to papers_url, alert: "Sorry, Paper could not be deleted !" }
        format.json { head :no_content }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_paper
      begin
        @paper = current_user.papers.find(params[:id])
      rescue ActiveRecord::RecordNotFound
        redirect_to papers_path, alert: "You are not authorized to perform this action."
      end
    end

    # Only allow a list of trusted parameters through.
    def paper_params
      params.require(:paper).permit(:title, :abstract, :submission_date, :pdf_link, :decision)
    end

    def valid_paper_of_current_user
      begin
        current_user.papers.find(params[:id]) 
      rescue ActiveRecord::RecordNotFound
        redirect_to papers_path, alert: "You are not authorized to perform this action."
      end
    end

end
