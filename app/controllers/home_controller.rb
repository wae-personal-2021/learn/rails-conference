class HomeController < ApplicationController
  def index
  end

  def all_papers
    @papers = Paper.all
  end
end
